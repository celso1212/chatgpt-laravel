<?php

use App\Http\Controllers\ChatGPTController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/chatgpt', [ChatGPTController::class, 'index'])
    ->name('chatgpt.index');
Route::post('/chatgpt/ask', [ChatGPTController::class, 'ask'])
    ->name('chatgpt.ask');

//rota para respostas e visualizar o banco
Route::get('/chatgpt/responses', [ChatGPTController::class, 'responses'])->name('chatgpt.responses');

//nova rota para a página de visualização de conversa no arquivo web.php:
Route::get('/chatgpt/conversation', [ChatGPTController::class, 'conversation'])->name('chatgpt.conversation');

//Rota para deletar do banco 
Route::delete('/chatgpt/{chatGPTResponse}', [ChatGPTController::class, 'destroy'])->name('chatgpt.destroy');
