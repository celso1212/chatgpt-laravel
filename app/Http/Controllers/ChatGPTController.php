<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\ChatGPTResponse;
use Illuminate\Support\Facades\Auth;

class ChatGPTController extends Controller
{
    /**
     * Exibe a página inicial do ChatGPT.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {   
        $responseCount = ChatGPTResponse::count();

        return view('chatgpt.index', ['responseCount' => $responseCount]);

    }

    /**
     * Envia uma solicitação para o modelo ChatGPT e salva a resposta em um banco de dados ou arquivo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function ask(Request $request)
    {
        // Obter o texto da solicitação do formulário
        $prompt = $request->input('prompt');

        // Enviar a solicitação para o modelo ChatGPT
        $response = $this->askToChatGPT($prompt);

        // Salvar a resposta no banco de dados
        $chatGPTResponse = new ChatGPTResponse();
        $chatGPTResponse->user_id = Auth::id();
        $chatGPTResponse->prompt = $prompt;
        $chatGPTResponse->response = $response;
        $chatGPTResponse->save();

        // Retornar uma resposta para a exibição na tela
        return view('chatgpt.response', ['response' => $response]);
    }

    /**
     * Envia uma solicitação para o modelo ChatGPT e retorna a resposta.
     *
     * @param  string  $prompt
     * @return string
     */
    private function askToChatGPT($prompt)
    {
        $response = Http::withoutVerifying()
            ->withHeaders([
                'Authorization' => 'Bearer ' . env('CHATGPT_API_KEY'),
                'Content-Type' => 'application/json',
            ])->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                "prompt" => $prompt,
                "max_tokens" => 1000,
                "temperature" => 0.5
            ]);

        return $response->json()['choices'][0]['text'];
    }

    /**
     * Exibe as respostas salvas no banco de dados.
     *
     * @return \Illuminate\View\View
     */
    public function responses()
    {
        $user_id = Auth::id();
        $responses = ChatGPTResponse::with('user')->where('user_id', $user_id)->latest()->paginate(10);
        $responseCount = ChatGPTResponse::where('user_id', $user_id)->count();
        return view('chatgpt.responses', compact('responses', 'responseCount'));
    }
    

    /**
     * Exibe a conversa do usuário atual com o ChatGPT.
     *
     * @return \Illuminate\View\View
     */
    public function conversation()
    {
        $user_id = Auth::id();
        $chatGPTResponses = ChatGPTResponse::where('user_id', $user_id)->get();

        return view('chatgpt.conversation', ['chatGPTResponses' => $chatGPTResponses]);
    }

    /**
     * Exclui uma resposta do banco de dados.
     *
     * @param  \App\Models\ChatGPTResponse  $chatGPTResponse
     * @return \Illuminate\Http\RedirectResponse
     */
 
    public function destroy(ChatGPTResponse $chatGPTResponse)
    {
        $chatGPTResponse->delete();
        return redirect()->route('chatgpt.index')->with('success', 'Registro excluído com sucesso!');
    }
            public function user()
        {
            return $this->belongsTo(User::class);
        }

        public function userResponses()
{
    $user = Auth::user();
    $responses = $user->chatGPTResponses()->latest()->paginate(10);

    return view('chatgpt.user_responses', compact('responses'));
}


}
