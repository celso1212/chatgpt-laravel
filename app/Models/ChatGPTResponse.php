<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatGPTResponse extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'prompt',
        'response'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function conversation()
{
    $user_id = Auth::id();
    $chatGPTResponses = ChatGPTResponse::where('user_id', $user_id)->get();

    return view('chatgpt.conversation', ['chatGPTResponses' => $chatGPTResponses]);
}

}
?>